﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library;

namespace MainForm
{
    public partial class MainForm : Form
    {
        public List<League> Leagues;

        public LeagueForm LeagueForm;
        public ClubForm ClubForm;
        public MatchForm MatchForm;
        public FindForm FindSomeForm;
        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LeagueForm = new LeagueForm();
            ClubForm = new ClubForm();
            MatchForm = new MatchForm();
            FindSomeForm = new FindForm();

            LeagueForm.SetMother(this);
            ClubForm.SetMother(this);
            MatchForm.SetMother(this);
            FindSomeForm.SetMother(this);

            Leagues = new List<League>();

            saveFileDialog.Filter = "Football file (fbd)|*.fbd";
            openFileDialog.Filter = "Football file (fbd)|*.fbd";
        }

        private void CreateLeague_Click(object sender, EventArgs e)
        {
            League createdLeague = new League();
            Leagues.Add(createdLeague);
            CallLeagueEditForm(createdLeague, false);
        }
        public void UpdateLeagueList()
        {
            leagueList.Items.Clear();

            foreach (League league in Leagues)
            {
                leagueList.Items.Add(league);
            }

            UpdateClubList();
        }
        public void UpdateClubList()
        {
            clubList.Items.Clear();

            if (leagueList.SelectedIndex == -1)
            {
                return;
            }
            League league = Leagues[leagueList.SelectedIndex];
            List<Club> clubs = league.Clubs;

            foreach (Club club in clubs)
            {
                clubList.Items.Add(club);
            }
            UpdateMatchList();
        }
        public void UpdateMatchList()
        {
            matchList.Items.Clear();

            if (leagueList.SelectedIndex == -1)
            {
                return;
            }
            League league = Leagues[leagueList.SelectedIndex];
            List<Match> matches = league.Matches;

            foreach (Match match in matches)
            {
                matchList.Items.Add(match);
            }
        }
        private void leagueList_DoubleClick(object sender, EventArgs e)
        {
            int selectedIndex = leagueList.SelectedIndex;
            if (selectedIndex != -1)
            {
                CallLeagueEditForm(leagueList.SelectedItem as League, true);
            }
        }
        public void CallLeagueEditForm(League target, bool mode)
        {
            LeagueForm.SetMode(mode);
            LeagueForm.SetTarget(target);
            LeagueForm.ShowDialog();

            UpdateLeagueList();
        }
        public void CallClubEditForm(Club target, bool mode)
        {
            ClubForm.SetMode(mode);
            ClubForm.SetTarget(target);
            ClubForm.ShowDialog();

            UpdateClubList();
        }

        public void CallMatchEditForm(Match target, bool mode)
        {
            MatchForm.SetMode(mode);
            MatchForm.SetTarget(target);
            MatchForm.ShowDialog();

            UpdateMatchList();
        }
        private void CreateClub_Click(object sender, EventArgs e)
        {
            int selectedIndex = leagueList.SelectedIndex;
            if (selectedIndex == -1)
            {
                MessageBox.Show("Для того, щоб створити клуб, необхідно обрати лігу, до якої він буде належати", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            League league = Leagues[leagueList.SelectedIndex];
            Club club = new Club();
            league.Clubs.Add(club);
            CallClubEditForm(club, false);
        }

        private void CreateMatch_Click(object sender, EventArgs e)
        {
            int selectedIndex = leagueList.SelectedIndex;
            if (selectedIndex == -1)
            {
                MessageBox.Show("Для того, щоб створити матч, необхідно обрати лігу, де він буде проходити", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            League league = Leagues[leagueList.SelectedIndex];
            if (league.Clubs == null || league.Clubs.Count < 2)
            {
                MessageBox.Show("Для того, щоб створити матч ліга повина мати хоча б 2 клуби", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Match createdMatch = new Match();
            createdMatch.TimeOfMatch = DateTime.Now;
            createdMatch.League = league;
            league.Matches.Add(createdMatch);
            CallMatchEditForm(createdMatch, false);
        }

        public bool RemoveLeague(League target)
        {
            if (target.Clubs.Count > 0)
            {
                MessageBox.Show("Неможливо видалити лігу, яка має клуби!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                DialogResult dialogResult = MessageBox.Show("Видалити клуби?", "Видалити клуби?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.No)
                {
                    return false;
                }
                else
                {
                    SafeRemoveLeagues(target, target.Clubs.ToArray());
                    UpdateMatchList();
                }
            }
            Leagues.Remove(target);
            UpdateLeagueList();
            return true;
        }
        public void SafeRemoveLeagues(League target, params Club[] arr)
        {
            League league = Leagues[leagueList.SelectedIndex];
            for (int i = 0; i < arr.Length; i++)
            {
                Club club = arr[i];
                target.Clubs.Remove(club);
                for (int j = 0; j < league.Matches.Count; j++)
                {
                    Match match = league.Matches[j];
                    if (match.First == club)
                    {
                        match.First = Club.UnknownClub;
                    }
                    if (match.Second == club)
                    {
                        match.Second = Club.UnknownClub;
                    }
                }
            }
        }
        public bool RemoveClub(Club target)
        {
            League league = Leagues[leagueList.SelectedIndex];
            SafeRemoveLeagues(league, target);
            UpdateClubList();
            return true;
        }
        public bool RemoveMatch(Match target)
        {
            League league = Leagues[leagueList.SelectedIndex];
            league.Matches.Remove(target);
            return true;
        }
        private void clubList_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateClubList();
        }

        private void clubList_DoubleClick(object sender, EventArgs e)
        {
            int selectedIndex = clubList.SelectedIndex;
            if (selectedIndex != -1)
            {
                CallClubEditForm(clubList.SelectedItem as Club, true);
            }
        }

        private void matchList_DoubleClick(object sender, EventArgs e)
        {
            League league = Leagues[leagueList.SelectedIndex];
            int selectedIndex = matchList.SelectedIndex;
            if (selectedIndex != -1)
            {
                CallMatchEditForm(matchList.SelectedItem as Match, true);
            }
        }

        private void Find_Click(object sender, EventArgs e)
        {
            FindSomeForm.ShowDialog();
        }

        private void SaveAll_Click(object sender, EventArgs e)
        {
            FileSaver fileSaver = new FileSaver(Leagues);
            if (saveFileDialog.ShowDialog() != DialogResult.Cancel)
            {
                fileSaver.Save(saveFileDialog.FileName);

                MessageBox.Show("Файл успішно збережено!", "Повідомлення", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void LoadAll_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() != DialogResult.Cancel)
            {
                FileSaver fileSaver = FileSaver.Load(openFileDialog.FileName);
                Leagues = fileSaver.GetLeagues();
                UpdateLeagueList();

                UpdateMatchList();
                MessageBox.Show("Файл успішно завантажено!", "Повідомлення", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void matchList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
