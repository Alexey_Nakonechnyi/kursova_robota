﻿namespace MainForm
{
    partial class LeagueForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LeagueForm));
            this.ModeOfWork = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.clubName = new System.Windows.Forms.TextBox();
            this.yearOfFoundation = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.LeagueCount = new System.Windows.Forms.Label();
            this.deleteClub = new System.Windows.Forms.Button();
            this.Exit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.yearOfFoundation)).BeginInit();
            this.SuspendLayout();
            // 
            // ModeOfWork
            // 
            this.ModeOfWork.AutoSize = true;
            this.ModeOfWork.BackColor = System.Drawing.Color.White;
            this.ModeOfWork.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ModeOfWork.Location = new System.Drawing.Point(10, 11);
            this.ModeOfWork.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ModeOfWork.Name = "ModeOfWork";
            this.ModeOfWork.Size = new System.Drawing.Size(155, 24);
            this.ModeOfWork.TabIndex = 0;
            this.ModeOfWork.Text = "*Режим роботи*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(10, 50);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Назва ліги";
            // 
            // clubName
            // 
            this.clubName.BackColor = System.Drawing.Color.White;
            this.clubName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.clubName.Location = new System.Drawing.Point(14, 74);
            this.clubName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.clubName.Name = "clubName";
            this.clubName.Size = new System.Drawing.Size(163, 26);
            this.clubName.TabIndex = 2;
            this.clubName.TextChanged += new System.EventHandler(this.clubName_TextChanged);
            // 
            // yearOfFoundation
            // 
            this.yearOfFoundation.BackColor = System.Drawing.Color.White;
            this.yearOfFoundation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.yearOfFoundation.Location = new System.Drawing.Point(14, 124);
            this.yearOfFoundation.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.yearOfFoundation.Maximum = new decimal(new int[] {
            2050,
            0,
            0,
            0});
            this.yearOfFoundation.Minimum = new decimal(new int[] {
            1950,
            0,
            0,
            0});
            this.yearOfFoundation.Name = "yearOfFoundation";
            this.yearOfFoundation.Size = new System.Drawing.Size(162, 26);
            this.yearOfFoundation.TabIndex = 3;
            this.yearOfFoundation.Value = new decimal(new int[] {
            1950,
            0,
            0,
            0});
            this.yearOfFoundation.ValueChanged += new System.EventHandler(this.yearOfFoundation_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(10, 101);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Рік заснування";
            // 
            // LeagueCount
            // 
            this.LeagueCount.AutoSize = true;
            this.LeagueCount.BackColor = System.Drawing.Color.White;
            this.LeagueCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LeagueCount.Location = new System.Drawing.Point(14, 154);
            this.LeagueCount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LeagueCount.Name = "LeagueCount";
            this.LeagueCount.Size = new System.Drawing.Size(140, 20);
            this.LeagueCount.TabIndex = 5;
            this.LeagueCount.Text = "*Кількість клубів*";
            // 
            // deleteClub
            // 
            this.deleteClub.BackColor = System.Drawing.Color.White;
            this.deleteClub.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteClub.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.deleteClub.Location = new System.Drawing.Point(124, 189);
            this.deleteClub.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.deleteClub.Name = "deleteClub";
            this.deleteClub.Size = new System.Drawing.Size(106, 28);
            this.deleteClub.TabIndex = 6;
            this.deleteClub.Text = "Видалити";
            this.deleteClub.UseVisualStyleBackColor = false;
            this.deleteClub.Click += new System.EventHandler(this.deleteClub_Click);
            // 
            // Exit
            // 
            this.Exit.BackColor = System.Drawing.Color.White;
            this.Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Exit.Location = new System.Drawing.Point(14, 189);
            this.Exit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(106, 28);
            this.Exit.TabIndex = 7;
            this.Exit.Text = "Зберегти";
            this.Exit.UseVisualStyleBackColor = false;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // LeagueForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(248, 235);
            this.ControlBox = false;
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.deleteClub);
            this.Controls.Add(this.LeagueCount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.yearOfFoundation);
            this.Controls.Add(this.clubName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ModeOfWork);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(264, 274);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(264, 274);
            this.Name = "LeagueForm";
            this.ShowInTaskbar = false;
            this.Text = "*Режим роботи*";
            ((System.ComponentModel.ISupportInitialize)(this.yearOfFoundation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ModeOfWork;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox clubName;
        private System.Windows.Forms.NumericUpDown yearOfFoundation;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LeagueCount;
        private System.Windows.Forms.Button deleteClub;
        private System.Windows.Forms.Button Exit;
    }
}