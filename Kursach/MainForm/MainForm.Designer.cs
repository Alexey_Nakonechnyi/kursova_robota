﻿namespace MainForm
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.clubList = new System.Windows.Forms.ListBox();
            this.matchList = new System.Windows.Forms.ListBox();
            this.leagueList = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.CreateLeague = new System.Windows.Forms.Button();
            this.CreateClub = new System.Windows.Forms.Button();
            this.CreateMatch = new System.Windows.Forms.Button();
            this.Find = new System.Windows.Forms.Button();
            this.SaveAll = new System.Windows.Forms.Button();
            this.LoadAll = new System.Windows.Forms.Button();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(74, 55);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ліги";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(268, 55);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Клуби";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(561, 55);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 24);
            this.label3.TabIndex = 2;
            this.label3.Text = "Матчі";
            // 
            // clubList
            // 
            this.clubList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.clubList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.clubList.FormattingEnabled = true;
            this.clubList.HorizontalScrollbar = true;
            this.clubList.ItemHeight = 20;
            this.clubList.Location = new System.Drawing.Point(218, 81);
            this.clubList.Margin = new System.Windows.Forms.Padding(2);
            this.clubList.Name = "clubList";
            this.clubList.Size = new System.Drawing.Size(145, 284);
            this.clubList.TabIndex = 4;
            this.clubList.DoubleClick += new System.EventHandler(this.clubList_DoubleClick);
            // 
            // matchList
            // 
            this.matchList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.matchList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.matchList.FormattingEnabled = true;
            this.matchList.HorizontalScrollbar = true;
            this.matchList.ItemHeight = 20;
            this.matchList.Location = new System.Drawing.Point(406, 81);
            this.matchList.Margin = new System.Windows.Forms.Padding(2);
            this.matchList.Name = "matchList";
            this.matchList.Size = new System.Drawing.Size(360, 284);
            this.matchList.TabIndex = 5;
            this.matchList.SelectedIndexChanged += new System.EventHandler(this.matchList_SelectedIndexChanged);
            this.matchList.DoubleClick += new System.EventHandler(this.matchList_DoubleClick);
            // 
            // leagueList
            // 
            this.leagueList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.leagueList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.leagueList.FormattingEnabled = true;
            this.leagueList.HorizontalScrollbar = true;
            this.leagueList.ItemHeight = 20;
            this.leagueList.Location = new System.Drawing.Point(34, 81);
            this.leagueList.Margin = new System.Windows.Forms.Padding(2);
            this.leagueList.Name = "leagueList";
            this.leagueList.Size = new System.Drawing.Size(145, 284);
            this.leagueList.TabIndex = 6;
            this.leagueList.SelectedIndexChanged += new System.EventHandler(this.clubList_SelectedIndexChanged);
            this.leagueList.DoubleClick += new System.EventHandler(this.leagueList_DoubleClick);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(256, 7);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(293, 37);
            this.label4.TabIndex = 7;
            this.label4.Text = "Футбольні змагання";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CreateLeague
            // 
            this.CreateLeague.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CreateLeague.BackColor = System.Drawing.Color.White;
            this.CreateLeague.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreateLeague.Location = new System.Drawing.Point(34, 375);
            this.CreateLeague.Margin = new System.Windows.Forms.Padding(2);
            this.CreateLeague.Name = "CreateLeague";
            this.CreateLeague.Size = new System.Drawing.Size(144, 32);
            this.CreateLeague.TabIndex = 8;
            this.CreateLeague.Text = "Створити лігу";
            this.CreateLeague.UseVisualStyleBackColor = false;
            this.CreateLeague.Click += new System.EventHandler(this.CreateLeague_Click);
            // 
            // CreateClub
            // 
            this.CreateClub.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CreateClub.BackColor = System.Drawing.Color.White;
            this.CreateClub.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreateClub.Location = new System.Drawing.Point(218, 375);
            this.CreateClub.Margin = new System.Windows.Forms.Padding(2);
            this.CreateClub.Name = "CreateClub";
            this.CreateClub.Size = new System.Drawing.Size(144, 32);
            this.CreateClub.TabIndex = 9;
            this.CreateClub.Text = "Створити клуб";
            this.CreateClub.UseVisualStyleBackColor = false;
            this.CreateClub.Click += new System.EventHandler(this.CreateClub_Click);
            // 
            // CreateMatch
            // 
            this.CreateMatch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CreateMatch.BackColor = System.Drawing.Color.White;
            this.CreateMatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreateMatch.Location = new System.Drawing.Point(406, 375);
            this.CreateMatch.Margin = new System.Windows.Forms.Padding(2);
            this.CreateMatch.Name = "CreateMatch";
            this.CreateMatch.Size = new System.Drawing.Size(358, 32);
            this.CreateMatch.TabIndex = 10;
            this.CreateMatch.Text = "Створити матч";
            this.CreateMatch.UseVisualStyleBackColor = false;
            this.CreateMatch.Click += new System.EventHandler(this.CreateMatch_Click);
            // 
            // Find
            // 
            this.Find.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Find.BackColor = System.Drawing.Color.White;
            this.Find.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Find.Location = new System.Drawing.Point(262, 418);
            this.Find.Margin = new System.Windows.Forms.Padding(2);
            this.Find.Name = "Find";
            this.Find.Size = new System.Drawing.Size(248, 32);
            this.Find.TabIndex = 11;
            this.Find.Text = "Знайти";
            this.Find.UseVisualStyleBackColor = false;
            this.Find.Click += new System.EventHandler(this.Find_Click);
            // 
            // SaveAll
            // 
            this.SaveAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SaveAll.BackColor = System.Drawing.Color.White;
            this.SaveAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveAll.Location = new System.Drawing.Point(34, 418);
            this.SaveAll.Margin = new System.Windows.Forms.Padding(2);
            this.SaveAll.Name = "SaveAll";
            this.SaveAll.Size = new System.Drawing.Size(144, 32);
            this.SaveAll.TabIndex = 12;
            this.SaveAll.Text = "Зберегти";
            this.SaveAll.UseVisualStyleBackColor = false;
            this.SaveAll.Click += new System.EventHandler(this.SaveAll_Click);
            // 
            // LoadAll
            // 
            this.LoadAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.LoadAll.BackColor = System.Drawing.Color.White;
            this.LoadAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LoadAll.Location = new System.Drawing.Point(620, 418);
            this.LoadAll.Margin = new System.Windows.Forms.Padding(2);
            this.LoadAll.Name = "LoadAll";
            this.LoadAll.Size = new System.Drawing.Size(144, 32);
            this.LoadAll.TabIndex = 13;
            this.LoadAll.Text = "Завантажити";
            this.LoadAll.UseVisualStyleBackColor = false;
            this.LoadAll.Click += new System.EventHandler(this.LoadAll_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "someFile";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(812, 461);
            this.Controls.Add(this.LoadAll);
            this.Controls.Add(this.SaveAll);
            this.Controls.Add(this.Find);
            this.Controls.Add(this.CreateMatch);
            this.Controls.Add(this.CreateClub);
            this.Controls.Add(this.CreateLeague);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.leagueList);
            this.Controls.Add(this.matchList);
            this.Controls.Add(this.clubList);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox clubList;
        private System.Windows.Forms.ListBox matchList;
        private System.Windows.Forms.ListBox leagueList;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button CreateLeague;
        private System.Windows.Forms.Button CreateClub;
        private System.Windows.Forms.Button CreateMatch;
        private System.Windows.Forms.Button Find;
        private System.Windows.Forms.Button SaveAll;
        private System.Windows.Forms.Button LoadAll;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
    }
}

