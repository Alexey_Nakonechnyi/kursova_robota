﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library;

namespace MainForm
{
    public partial class LeagueForm : Form
    {
        private League targetClub;
        private MainForm mother;
        public LeagueForm()
        {
            InitializeComponent();
        }
        public void SetMother(MainForm mother)
        {
            this.mother = mother;
        }
        public void SetMode(bool isEdit)
        {
            string mode = (isEdit)? "Редагування ліги": "Створення ліги";
            deleteClub.Text = (isEdit) ? "Видалити" : "Скасувати";
            ModeOfWork.Text = mode;
            Text = mode;
        }
        public void SetTarget(League target)
        {
            targetClub = target;

            if (target.YearOfFoundation > 2050)
            {
                target.YearOfFoundation = 2050;
            }
            if (target.YearOfFoundation < 1950)
            {
                target.YearOfFoundation = 1950;
            }
            UpdateInformationAboutTarget();
        }
        public void UpdateInformationAboutTarget()
        {
            if (targetClub == null)
            {
                return;
            }
            clubName.Text = targetClub.Name;
            yearOfFoundation.Value = targetClub.YearOfFoundation;
            // ==>> Можливо необхідно переписати?
            LeagueCount.Text = $"Кількість клубів: {targetClub.Clubs?.Count ?? 0}";
        }
        private void Exit_Click(object sender, EventArgs e)
        {
            if (clubName.Text.Length == 0)
            {
                MessageBox.Show("Ліга повина мати назву!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Hide();
        }

        private void clubName_TextChanged(object sender, EventArgs e)
        {
            if (targetClub != null)
            {
                targetClub.Name = clubName.Text;
            }
        }

        private void yearOfFoundation_ValueChanged(object sender, EventArgs e)
        {
            if(targetClub != null)
            {
                targetClub.YearOfFoundation = (int)yearOfFoundation.Value;
            }
        }

        private void deleteClub_Click(object sender, EventArgs e)
        {
            if (mother.RemoveLeague(targetClub))
            {
                Hide();
            }
        }
    }
}
