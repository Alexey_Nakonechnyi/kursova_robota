﻿namespace MainForm
{
    partial class MatchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MatchForm));
            this.matchName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ModeOfWork = new System.Windows.Forms.Label();
            this.matchTime = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ClubListOne = new System.Windows.Forms.ListBox();
            this.ClubListSecond = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Save = new System.Windows.Forms.Button();
            this.deleteMatch = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // matchName
            // 
            this.matchName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.matchName.Location = new System.Drawing.Point(13, 54);
            this.matchName.Margin = new System.Windows.Forms.Padding(2);
            this.matchName.Name = "matchName";
            this.matchName.Size = new System.Drawing.Size(163, 26);
            this.matchName.TabIndex = 5;
            this.matchName.TextChanged += new System.EventHandler(this.matchName_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(9, 31);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Ім\'я матча";
            // 
            // ModeOfWork
            // 
            this.ModeOfWork.AutoSize = true;
            this.ModeOfWork.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ModeOfWork.Location = new System.Drawing.Point(9, 7);
            this.ModeOfWork.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ModeOfWork.Name = "ModeOfWork";
            this.ModeOfWork.Size = new System.Drawing.Size(155, 24);
            this.ModeOfWork.TabIndex = 3;
            this.ModeOfWork.Text = "*Режим роботи*";
            // 
            // matchTime
            // 
            this.matchTime.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.matchTime.Location = new System.Drawing.Point(13, 103);
            this.matchTime.Margin = new System.Windows.Forms.Padding(2);
            this.matchTime.Name = "matchTime";
            this.matchTime.Size = new System.Drawing.Size(163, 20);
            this.matchTime.TabIndex = 6;
            this.matchTime.ValueChanged += new System.EventHandler(this.matchTime_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(9, 80);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Дата матчу ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(196, 7);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "Перша команда:";
            // 
            // ClubListOne
            // 
            this.ClubListOne.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ClubListOne.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClubListOne.FormattingEnabled = true;
            this.ClubListOne.ItemHeight = 20;
            this.ClubListOne.Location = new System.Drawing.Point(200, 34);
            this.ClubListOne.Margin = new System.Windows.Forms.Padding(2);
            this.ClubListOne.Name = "ClubListOne";
            this.ClubListOne.Size = new System.Drawing.Size(163, 164);
            this.ClubListOne.TabIndex = 9;
            this.ClubListOne.SelectedIndexChanged += new System.EventHandler(this.ClubListOne_SelectedIndexChanged);
            // 
            // ClubListSecond
            // 
            this.ClubListSecond.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ClubListSecond.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClubListSecond.FormattingEnabled = true;
            this.ClubListSecond.ItemHeight = 20;
            this.ClubListSecond.Location = new System.Drawing.Point(367, 34);
            this.ClubListSecond.Margin = new System.Windows.Forms.Padding(2);
            this.ClubListSecond.Name = "ClubListSecond";
            this.ClubListSecond.Size = new System.Drawing.Size(163, 164);
            this.ClubListSecond.TabIndex = 11;
            this.ClubListSecond.SelectedIndexChanged += new System.EventHandler(this.ClubListSecond_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(363, 7);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(127, 20);
            this.label4.TabIndex = 10;
            this.label4.Text = "Друга команда:";
            // 
            // Save
            // 
            this.Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Save.Location = new System.Drawing.Point(13, 136);
            this.Save.Margin = new System.Windows.Forms.Padding(2);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(122, 30);
            this.Save.TabIndex = 12;
            this.Save.Text = "Зберегти";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // deleteMatch
            // 
            this.deleteMatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deleteMatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.deleteMatch.Location = new System.Drawing.Point(13, 171);
            this.deleteMatch.Margin = new System.Windows.Forms.Padding(2);
            this.deleteMatch.Name = "deleteMatch";
            this.deleteMatch.Size = new System.Drawing.Size(122, 30);
            this.deleteMatch.TabIndex = 13;
            this.deleteMatch.Text = "Видалити";
            this.deleteMatch.UseVisualStyleBackColor = true;
            this.deleteMatch.Click += new System.EventHandler(this.deleteMatch_Click);
            // 
            // MatchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(542, 244);
            this.ControlBox = false;
            this.Controls.Add(this.deleteMatch);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.ClubListSecond);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ClubListOne);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.matchTime);
            this.Controls.Add(this.matchName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ModeOfWork);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximumSize = new System.Drawing.Size(558, 7320);
            this.MinimumSize = new System.Drawing.Size(558, 260);
            this.Name = "MatchForm";
            this.ShowInTaskbar = false;
            this.Text = "MatchForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox matchName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label ModeOfWork;
        private System.Windows.Forms.DateTimePicker matchTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox ClubListOne;
        private System.Windows.Forms.ListBox ClubListSecond;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Button deleteMatch;
    }
}