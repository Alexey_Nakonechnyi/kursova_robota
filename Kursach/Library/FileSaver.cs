﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    [Serializable]
    public class FileSaver
    {
        private List<League> leagues;
        public FileSaver(List<League> clubs)
        {
            this.leagues = clubs;
        }
        public List<League> GetLeagues()
        {
            return leagues;
        }
        public void Save(string path)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, this);
            }
        }
        public static FileSaver Load(string path)
        {
            FileSaver fileSaver = null;
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                fileSaver = formatter.Deserialize(fs) as FileSaver;
            }
            return fileSaver;
        }
    }
}
