﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Library;

namespace MainForm
{
    public partial class FindForm : Form
    {
        private MainForm mother;
        private int findMode = 0;
        public FindForm()
        {
            InitializeComponent();

            radioFindClub.Checked = true;
            radioFindName.Checked = true;
        }
        public void SetMother(MainForm mother)
        {
            this.mother = mother;
        }

        private void returnButton_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void radioFindLeague_CheckedChanged(object sender, EventArgs e)
        {
            if (radioFindClub.Checked)
            {
                radioFindLeagueName.Enabled = false;
                if (radioFindLeagueName.Checked)
                {
                    radioFindName.Checked = true;
                }
                radioFindDate.Text = "роком заснування";
                CheckDateFields();
            }
        }

        private void radioFindClub_CheckedChanged(object sender, EventArgs e)
        {
            if (radioFindLeague.Checked)
            {
                radioFindLeagueName.Enabled = false;
                if (radioFindLeagueName.Checked)
                {
                    radioFindName.Checked = true;
                }
                radioFindDate.Text = "роком заснування";
                CheckDateFields();
            }
        }

        private void radioFindMatch_CheckedChanged(object sender, EventArgs e)
        {
            if (radioFindMatch.Checked)
            {
                radioFindLeagueName.Enabled = true;
                radioFindDate.Text = "датою матча";
                CheckDateFields();
            }
        }

        private void radioFindName_CheckedChanged(object sender, EventArgs e)
        {
            if (radioFindName.Checked)
            {
                equalsDate.Enabled = false;

                findDateField.Enabled = false;
                findDateYearField.Enabled = false;
                findNameField.Enabled = true;
            }
        }

        private void radioFindDate_CheckedChanged(object sender, EventArgs e)
        {
            if (radioFindDate.Checked)
            {
                equalsDate.Enabled = true;
                CheckDateFields();
                findNameField.Enabled = false;
            }
        }
        private void CheckDateFields()
        {
            if (radioFindDate.Checked)
            {
                if (radioFindMatch.Checked)
                {
                    findDateField.Enabled = true;
                    findDateYearField.Enabled = false;
                }
                else
                {
                    findDateField.Enabled = false;
                    findDateYearField.Enabled = true;
                }
            }
        }
        private void radioFindClubName_CheckedChanged(object sender, EventArgs e)
        {
            if (radioFindLeagueName.Checked)
            {
                equalsDate.Enabled = false;

                findDateField.Enabled = false;
                findDateYearField.Enabled = false;
                findNameField.Enabled = true;
            }
        }

        private void findButton_Click(object sender, EventArgs e)
        {
            findResultList.Items.Clear();
            if (radioFindClub.Checked)
            {
                findMode = 0;
                findLeaguAction();
            }
            else if (radioFindLeague.Checked)
            {
                findMode = 1;
                findClubAction();
            }
            else
            {
                findMode = 2;
                findMatchAction();
            }
        }
        private void findLeaguAction()
        {
            List<League> leagues = mother.Leagues;
            if (radioFindName.Checked)
            {
                string target = findNameField.Text;
                foreach (League league in leagues)
                {
                    if (league.Name == target)
                    {
                        findResultList.Items.Add(league);
                    }
                }
            }
            else
            {
                foreach (League league in leagues)
                {
                    int compareResult = compareSimpleDate(league.YearOfFoundation, (int)findDateYearField.Value);
                    bool needToAdd = false;
                    switch (compareResult)
                    {
                        case -1:
                            if (beforeDate.Checked)
                            {
                                needToAdd = true;
                            }
                            break;
                        case 0:
                            if (equivalentDate.Checked)
                            {
                                needToAdd = true;
                            }
                            break;
                        case 1:
                            if (afterDate.Checked)
                            {
                                needToAdd = true;
                            }
                            break;
                    }
                    if (needToAdd)
                    {
                        findResultList.Items.Add(league);
                    }
                }

            }
        }
        private void findClubAction()
        {
            List<Club> clubs = Club.GenerateClubList(mother.Leagues);
            if (radioFindName.Checked)
            {
                string target = findNameField.Text;
                foreach (Club club in clubs)
                {
                    if (club.Name == target)
                    {
                        findResultList.Items.Add(club);
                    }
                }
            }
            else
            {
                foreach (Club club in clubs)
                {
                    int compareResult = compareSimpleDate(club.YearOfFoundation, (int)findDateYearField.Value);
                    bool needToAdd = false;
                    switch (compareResult)
                    {
                        case -1:
                            if (beforeDate.Checked)
                            {
                                needToAdd = true;
                            }
                            break;
                        case 0:
                            if (equivalentDate.Checked)
                            {
                                needToAdd = true;
                            }
                            break;
                        case 1:
                            if (afterDate.Checked)
                            {
                                needToAdd = true;
                            }
                            break;
                    }
                    if (needToAdd)
                    {
                        findResultList.Items.Add(club);
                    }
                }

            }
        }
        private void findMatchAction()
        {
            List<Match> matches = Match.GenerateClubList(mother.Leagues);
            if (radioFindName.Checked)
            {
                string target = findNameField.Text;
                foreach (Match match in matches)
                {
                    if (match.Name == target)
                    {
                        findResultList.Items.Add(match);
                    }
                }
            }
            else if (radioFindDate.Checked)
            {
                foreach (Match match in matches)
                {
                    if (beforeDate.Checked && compareDateSimple(match.TimeOfMatch, findDateField.Value) == -1 ||
                        equivalentDate.Checked && compareDateSimple(match.TimeOfMatch, findDateField.Value) == 0 ||
                        afterDate.Checked && compareDateSimple(match.TimeOfMatch, findDateField.Value) == 1)
                    {
                        findResultList.Items.Add(match);
                    }
                }
            }
            else
            {
                string target = findNameField.Text;
                foreach (Match match in matches)
                {
                    if (match.First.Name == target)
                    {
                        findResultList.Items.Add(match);
                    }
                    else if (match.Second.Name == target)
                    {
                        findResultList.Items.Add(match);
                    }
                }
            }
        }
        private int compareSimpleDate(int first, int second)
        {
            if (first == second)
            {
                return 0;
            }
            else if (first > second)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }
        private int compareDateSimple(DateTime first, DateTime second)
        {
            first = new DateTime(first.Year, first.Month, first.Day);
            second = new DateTime(second.Year, second.Month, second.Day);
            if (first == second)
            {
                return 0;
            }
            else if (first > second)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }

        private void findResultList_DoubleClick(object sender, EventArgs e)
        {
            int index = findResultList.SelectedIndex;
            if (index != -1)
            {
                object target = findResultList.SelectedItem;
                switch (findMode)
                {
                    case 0:
                        mother.CallLeagueEditForm(target as League, true);
                        break;
                    case 1:
                        mother.CallClubEditForm(target as Club, true);
                        break;
                    case 2:
                        mother.CallMatchEditForm(target as Match, true);
                        break;
                }
            }
        }
    }
}
