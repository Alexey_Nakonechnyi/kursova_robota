﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Windows.Forms;
using Library;

namespace MainForm
{
    public partial class MatchForm : Form
    {
        private Match targetMatch;
        private MainForm mother;
        public MatchForm()
        {
            InitializeComponent();
        }
        public void SetMother(MainForm mother)
        {
            this.mother = mother;
        }
        public void SetMode(bool isEdit)
        {
            string mode = (isEdit) ? "Редагування гри" : "Створення гри";
            deleteMatch.Text = (isEdit) ? "Видалити": "Скасувати";
            ModeOfWork.Text = mode;
            Text = mode;
        }
        public void SetTarget(Match target)
        {
            targetMatch = target;

            UpdateInformationAboutTarget();
        }
        public void UpdateInformationAboutTarget()
        {
            if (targetMatch == null)
            {
                return;
            }
            if (targetMatch.TimeOfMatch < matchTime.MinDate)
            {
                targetMatch.TimeOfMatch = matchTime.MinDate;
            } else if (targetMatch.TimeOfMatch > matchTime.MaxDate)
            {
                targetMatch.TimeOfMatch = matchTime.MaxDate;
            }
            matchName.Text = targetMatch.Name;
            matchTime.Value = targetMatch.TimeOfMatch;
            UpdateLists();
        }
        public void UpdateLists()
        {
            ClubListOne.Items.Clear();
            ClubListSecond.Items.Clear();

            List<Club> clubs = targetMatch.League.Clubs;
            foreach (Club club in clubs)
            {
                ClubListOne.Items.Add(club);
                ClubListSecond.Items.Add(club);
            }

            ClubListOne.SelectedItem = targetMatch.First;
            ClubListSecond.SelectedItem = targetMatch.Second;
        }

        private void Save_Click(object sender, EventArgs e)
        {
            if (ClubListOne.SelectedItem == ClubListSecond.SelectedItem && ClubListOne.SelectedItem != null)
            {
                MessageBox.Show("Оберіть різні клуби!", "Повідомлення", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (matchName.Text.Length == 0)
            {
                MessageBox.Show("Матч повинен мати назву!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Hide();
        }

        private void matchName_TextChanged(object sender, EventArgs e)
        {
            if (targetMatch != null)
            {
                targetMatch.Name = matchName.Text;
            }
        }

        private void matchTime_ValueChanged(object sender, EventArgs e)
        {
            if (targetMatch != null)
            {
                targetMatch.TimeOfMatch = matchTime.Value;
            }
        }

        private void ClubListOne_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (targetMatch != null)
            {
                targetMatch.First = ClubListOne.SelectedItem as Club;
            }
        }

        private void ClubListSecond_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (targetMatch != null)
            {
                targetMatch.Second = ClubListSecond.SelectedItem as Club;
            }
        }

        private void deleteMatch_Click(object sender, EventArgs e)
        {
            if (mother.RemoveMatch(targetMatch))
            {
                Hide();
            }
        }
    }
}
