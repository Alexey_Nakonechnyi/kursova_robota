﻿namespace MainForm
{
    partial class FindForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FindForm));
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioFindMatch = new System.Windows.Forms.RadioButton();
            this.radioFindLeague = new System.Windows.Forms.RadioButton();
            this.radioFindClub = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radioFindLeagueName = new System.Windows.Forms.RadioButton();
            this.radioFindDate = new System.Windows.Forms.RadioButton();
            this.radioFindName = new System.Windows.Forms.RadioButton();
            this.equalsDate = new System.Windows.Forms.Panel();
            this.afterDate = new System.Windows.Forms.RadioButton();
            this.equivalentDate = new System.Windows.Forms.RadioButton();
            this.beforeDate = new System.Windows.Forms.RadioButton();
            this.returnButton = new System.Windows.Forms.Button();
            this.findDateYearField = new System.Windows.Forms.NumericUpDown();
            this.findNameField = new System.Windows.Forms.TextBox();
            this.findDateField = new System.Windows.Forms.DateTimePicker();
            this.findResultList = new System.Windows.Forms.ListBox();
            this.findButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.equalsDate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.findDateYearField)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(10, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Знайти....";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.radioFindMatch);
            this.panel1.Controls.Add(this.radioFindLeague);
            this.panel1.Controls.Add(this.radioFindClub);
            this.panel1.Location = new System.Drawing.Point(10, 37);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(131, 87);
            this.panel1.TabIndex = 1;
            // 
            // radioFindMatch
            // 
            this.radioFindMatch.AutoSize = true;
            this.radioFindMatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.radioFindMatch.Location = new System.Drawing.Point(4, 59);
            this.radioFindMatch.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radioFindMatch.Name = "radioFindMatch";
            this.radioFindMatch.Size = new System.Drawing.Size(67, 24);
            this.radioFindMatch.TabIndex = 2;
            this.radioFindMatch.Text = "Матч";
            this.radioFindMatch.UseVisualStyleBackColor = true;
            this.radioFindMatch.CheckedChanged += new System.EventHandler(this.radioFindMatch_CheckedChanged);
            // 
            // radioFindLeague
            // 
            this.radioFindLeague.AutoSize = true;
            this.radioFindLeague.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.radioFindLeague.Location = new System.Drawing.Point(4, 31);
            this.radioFindLeague.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radioFindLeague.Name = "radioFindLeague";
            this.radioFindLeague.Size = new System.Drawing.Size(63, 24);
            this.radioFindLeague.TabIndex = 1;
            this.radioFindLeague.Text = "Клуб";
            this.radioFindLeague.UseVisualStyleBackColor = true;
            this.radioFindLeague.CheckedChanged += new System.EventHandler(this.radioFindClub_CheckedChanged);
            // 
            // radioFindClub
            // 
            this.radioFindClub.AutoSize = true;
            this.radioFindClub.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.radioFindClub.Location = new System.Drawing.Point(4, 2);
            this.radioFindClub.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radioFindClub.Name = "radioFindClub";
            this.radioFindClub.Size = new System.Drawing.Size(56, 24);
            this.radioFindClub.TabIndex = 0;
            this.radioFindClub.Text = "Лігу";
            this.radioFindClub.UseVisualStyleBackColor = true;
            this.radioFindClub.CheckedChanged += new System.EventHandler(this.radioFindLeague_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(220, 10);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "Шукати за.....";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.radioFindLeagueName);
            this.panel2.Controls.Add(this.radioFindDate);
            this.panel2.Controls.Add(this.radioFindName);
            this.panel2.Location = new System.Drawing.Point(220, 37);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(229, 87);
            this.panel2.TabIndex = 3;
            // 
            // radioFindLeagueName
            // 
            this.radioFindLeagueName.AutoSize = true;
            this.radioFindLeagueName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.radioFindLeagueName.Location = new System.Drawing.Point(4, 59);
            this.radioFindLeagueName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radioFindLeagueName.Name = "radioFindLeagueName";
            this.radioFindLeagueName.Size = new System.Drawing.Size(210, 24);
            this.radioFindLeagueName.TabIndex = 3;
            this.radioFindLeagueName.Text = "назвою одноєї з команд";
            this.radioFindLeagueName.UseVisualStyleBackColor = true;
            this.radioFindLeagueName.CheckedChanged += new System.EventHandler(this.radioFindClubName_CheckedChanged);
            // 
            // radioFindDate
            // 
            this.radioFindDate.AutoSize = true;
            this.radioFindDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.radioFindDate.Location = new System.Drawing.Point(4, 31);
            this.radioFindDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radioFindDate.Name = "radioFindDate";
            this.radioFindDate.Size = new System.Drawing.Size(78, 24);
            this.radioFindDate.TabIndex = 1;
            this.radioFindDate.Text = "*Дата*";
            this.radioFindDate.UseVisualStyleBackColor = true;
            this.radioFindDate.CheckedChanged += new System.EventHandler(this.radioFindDate_CheckedChanged);
            // 
            // radioFindName
            // 
            this.radioFindName.AutoSize = true;
            this.radioFindName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.radioFindName.Location = new System.Drawing.Point(4, 2);
            this.radioFindName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radioFindName.Name = "radioFindName";
            this.radioFindName.Size = new System.Drawing.Size(79, 24);
            this.radioFindName.TabIndex = 0;
            this.radioFindName.Text = "іменем";
            this.radioFindName.UseVisualStyleBackColor = true;
            this.radioFindName.CheckedChanged += new System.EventHandler(this.radioFindName_CheckedChanged);
            // 
            // equalsDate
            // 
            this.equalsDate.BackColor = System.Drawing.Color.White;
            this.equalsDate.Controls.Add(this.afterDate);
            this.equalsDate.Controls.Add(this.equivalentDate);
            this.equalsDate.Controls.Add(this.beforeDate);
            this.equalsDate.Location = new System.Drawing.Point(10, 129);
            this.equalsDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.equalsDate.Name = "equalsDate";
            this.equalsDate.Size = new System.Drawing.Size(439, 34);
            this.equalsDate.TabIndex = 4;
            // 
            // afterDate
            // 
            this.afterDate.AutoSize = true;
            this.afterDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.afterDate.Location = new System.Drawing.Point(358, 2);
            this.afterDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.afterDate.Name = "afterDate";
            this.afterDate.Size = new System.Drawing.Size(84, 24);
            this.afterDate.TabIndex = 3;
            this.afterDate.Text = "Пізніше";
            this.afterDate.UseVisualStyleBackColor = true;
            // 
            // equivalentDate
            // 
            this.equivalentDate.AutoSize = true;
            this.equivalentDate.Checked = true;
            this.equivalentDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.equivalentDate.Location = new System.Drawing.Point(196, 2);
            this.equivalentDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.equivalentDate.Name = "equivalentDate";
            this.equivalentDate.Size = new System.Drawing.Size(67, 24);
            this.equivalentDate.TabIndex = 1;
            this.equivalentDate.TabStop = true;
            this.equivalentDate.Text = "Рівно";
            this.equivalentDate.UseVisualStyleBackColor = true;
            // 
            // beforeDate
            // 
            this.beforeDate.AutoSize = true;
            this.beforeDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.beforeDate.Location = new System.Drawing.Point(4, 2);
            this.beforeDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.beforeDate.Name = "beforeDate";
            this.beforeDate.Size = new System.Drawing.Size(80, 24);
            this.beforeDate.TabIndex = 0;
            this.beforeDate.Text = "Раніше";
            this.beforeDate.UseVisualStyleBackColor = true;
            // 
            // returnButton
            // 
            this.returnButton.BackColor = System.Drawing.Color.White;
            this.returnButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.returnButton.Location = new System.Drawing.Point(10, 324);
            this.returnButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.returnButton.Name = "returnButton";
            this.returnButton.Size = new System.Drawing.Size(439, 34);
            this.returnButton.TabIndex = 5;
            this.returnButton.Text = "Повернутися";
            this.returnButton.UseVisualStyleBackColor = false;
            this.returnButton.Click += new System.EventHandler(this.returnButton_Click);
            // 
            // findDateYearField
            // 
            this.findDateYearField.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.findDateYearField.Location = new System.Drawing.Point(10, 169);
            this.findDateYearField.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.findDateYearField.Maximum = new decimal(new int[] {
            2050,
            0,
            0,
            0});
            this.findDateYearField.Minimum = new decimal(new int[] {
            1950,
            0,
            0,
            0});
            this.findDateYearField.Name = "findDateYearField";
            this.findDateYearField.Size = new System.Drawing.Size(184, 26);
            this.findDateYearField.TabIndex = 6;
            this.findDateYearField.Value = new decimal(new int[] {
            1950,
            0,
            0,
            0});
            // 
            // findNameField
            // 
            this.findNameField.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.findNameField.Location = new System.Drawing.Point(10, 198);
            this.findNameField.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.findNameField.Name = "findNameField";
            this.findNameField.Size = new System.Drawing.Size(185, 26);
            this.findNameField.TabIndex = 7;
            // 
            // findDateField
            // 
            this.findDateField.Location = new System.Drawing.Point(10, 228);
            this.findDateField.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.findDateField.Name = "findDateField";
            this.findDateField.Size = new System.Drawing.Size(185, 20);
            this.findDateField.TabIndex = 8;
            // 
            // findResultList
            // 
            this.findResultList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.findResultList.BackColor = System.Drawing.Color.White;
            this.findResultList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.findResultList.FormattingEnabled = true;
            this.findResultList.HorizontalScrollbar = true;
            this.findResultList.ItemHeight = 20;
            this.findResultList.Location = new System.Drawing.Point(453, 10);
            this.findResultList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.findResultList.Name = "findResultList";
            this.findResultList.Size = new System.Drawing.Size(247, 344);
            this.findResultList.TabIndex = 9;
            this.findResultList.DoubleClick += new System.EventHandler(this.findResultList_DoubleClick);
            // 
            // findButton
            // 
            this.findButton.BackColor = System.Drawing.Color.White;
            this.findButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.findButton.Location = new System.Drawing.Point(10, 285);
            this.findButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.findButton.Name = "findButton";
            this.findButton.Size = new System.Drawing.Size(439, 34);
            this.findButton.TabIndex = 10;
            this.findButton.Text = "Знайти";
            this.findButton.UseVisualStyleBackColor = false;
            this.findButton.Click += new System.EventHandler(this.findButton_Click);
            // 
            // FindForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(708, 368);
            this.ControlBox = false;
            this.Controls.Add(this.findButton);
            this.Controls.Add(this.findResultList);
            this.Controls.Add(this.findDateField);
            this.Controls.Add(this.findNameField);
            this.Controls.Add(this.findDateYearField);
            this.Controls.Add(this.returnButton);
            this.Controls.Add(this.equalsDate);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FindForm";
            this.ShowInTaskbar = false;
            this.Text = "FindForm";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.equalsDate.ResumeLayout(false);
            this.equalsDate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.findDateYearField)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioFindMatch;
        private System.Windows.Forms.RadioButton radioFindLeague;
        private System.Windows.Forms.RadioButton radioFindClub;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton radioFindLeagueName;
        private System.Windows.Forms.RadioButton radioFindDate;
        private System.Windows.Forms.RadioButton radioFindName;
        private System.Windows.Forms.Panel equalsDate;
        private System.Windows.Forms.RadioButton afterDate;
        private System.Windows.Forms.RadioButton equivalentDate;
        private System.Windows.Forms.RadioButton beforeDate;
        private System.Windows.Forms.Button returnButton;
        private System.Windows.Forms.NumericUpDown findDateYearField;
        private System.Windows.Forms.TextBox findNameField;
        private System.Windows.Forms.DateTimePicker findDateField;
        private System.Windows.Forms.ListBox findResultList;
        private System.Windows.Forms.Button findButton;
    }
}