﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    [Serializable]
    public class Club
    {
        public static readonly Club UnknownClub = new Club() { name = "Unknown", yearOfFoundation = 2000};
        private string name;
        public string Name
        {
            get => name;
            set => name = value;
        }
        private int yearOfFoundation;
        public int YearOfFoundation
        {
            get => yearOfFoundation;
            set => yearOfFoundation = value;
        }
        public override string ToString()
        {
            return string.Copy(name);
        }
        public Club()
        {
            name = "";
        }
        public static List<Club> GenerateClubList(List<League> leagues)
        {
            List<Club> clubs = new List<Club>();
            foreach (League league in leagues)
            {
                clubs.AddRange(league.Clubs);
            }
            return clubs;
        }
    }
}
