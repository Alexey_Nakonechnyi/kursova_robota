﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library;

namespace MainForm
{
    public partial class ClubForm : Form
    {
        private Club targetLeague;
        private MainForm mother;
        public ClubForm()
        {
            InitializeComponent();
        }
        public void SetMother(MainForm mother)
        {
            this.mother = mother;
        }
        public void SetTarget(Club target)
        {
            targetLeague = target;

            if (target.YearOfFoundation > 2050)
            {
                target.YearOfFoundation = 2050;
            }
            if (target.YearOfFoundation < 1950)
            {
                target.YearOfFoundation = 1950;
            }
            UpdateInformationAboutTarget();
        }
        public void UpdateInformationAboutTarget()
        {
            if (targetLeague == null)
            {
                return;
            }
            leagueName.Text = targetLeague.Name;
            yearOfFoundation.Value = targetLeague.YearOfFoundation;
        }
        public void SetMode(bool isEdit)
        {
            string mode = (isEdit) ? "Редагування клубу" : "Створення клубу";
            deleteLeague.Text = (isEdit) ? "Видалити" : "Скасувати";
            ModeOfWork.Text = mode;
            Text = mode;
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            if (leagueName.Text.Length == 0)
            {
                MessageBox.Show("Клуб повинен мати назву!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Hide();
        }

        private void leagueName_TextChanged(object sender, EventArgs e)
        {
            if (targetLeague != null)
            {
                targetLeague.Name = leagueName.Text;
            }
        }

        private void yearOfFoundation_ValueChanged(object sender, EventArgs e)
        {
            if (targetLeague != null)
            {
                targetLeague.YearOfFoundation = (int)yearOfFoundation.Value;
            }
        }

        private void deleteLeague_Click(object sender, EventArgs e)
        {
            if (mother.RemoveClub(targetLeague))
            {
                Hide();
            }
        }
    }
}
