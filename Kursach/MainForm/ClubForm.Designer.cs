﻿namespace MainForm
{
    partial class ClubForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClubForm));
            this.Exit = new System.Windows.Forms.Button();
            this.deleteLeague = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.yearOfFoundation = new System.Windows.Forms.NumericUpDown();
            this.leagueName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ModeOfWork = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.yearOfFoundation)).BeginInit();
            this.SuspendLayout();
            // 
            // Exit
            // 
            this.Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Exit.Location = new System.Drawing.Point(13, 186);
            this.Exit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(106, 28);
            this.Exit.TabIndex = 14;
            this.Exit.Text = "Зберегти";
            this.Exit.UseVisualStyleBackColor = true;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // deleteLeague
            // 
            this.deleteLeague.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.deleteLeague.Location = new System.Drawing.Point(124, 186);
            this.deleteLeague.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.deleteLeague.Name = "deleteLeague";
            this.deleteLeague.Size = new System.Drawing.Size(106, 28);
            this.deleteLeague.TabIndex = 13;
            this.deleteLeague.Text = "Видалити";
            this.deleteLeague.UseVisualStyleBackColor = true;
            this.deleteLeague.Click += new System.EventHandler(this.deleteLeague_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(9, 98);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 20);
            this.label3.TabIndex = 12;
            this.label3.Text = "Рік заснування";
            // 
            // yearOfFoundation
            // 
            this.yearOfFoundation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.yearOfFoundation.Location = new System.Drawing.Point(13, 120);
            this.yearOfFoundation.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.yearOfFoundation.Maximum = new decimal(new int[] {
            2050,
            0,
            0,
            0});
            this.yearOfFoundation.Minimum = new decimal(new int[] {
            1950,
            0,
            0,
            0});
            this.yearOfFoundation.Name = "yearOfFoundation";
            this.yearOfFoundation.Size = new System.Drawing.Size(162, 26);
            this.yearOfFoundation.TabIndex = 11;
            this.yearOfFoundation.Value = new decimal(new int[] {
            1950,
            0,
            0,
            0});
            this.yearOfFoundation.ValueChanged += new System.EventHandler(this.yearOfFoundation_ValueChanged);
            // 
            // leagueName
            // 
            this.leagueName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.leagueName.Location = new System.Drawing.Point(13, 71);
            this.leagueName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.leagueName.Name = "leagueName";
            this.leagueName.Size = new System.Drawing.Size(163, 26);
            this.leagueName.TabIndex = 10;
            this.leagueName.TextChanged += new System.EventHandler(this.leagueName_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(9, 47);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Ім\'я клубу";
            // 
            // ModeOfWork
            // 
            this.ModeOfWork.AutoSize = true;
            this.ModeOfWork.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ModeOfWork.Location = new System.Drawing.Point(9, 7);
            this.ModeOfWork.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ModeOfWork.Name = "ModeOfWork";
            this.ModeOfWork.Size = new System.Drawing.Size(155, 24);
            this.ModeOfWork.TabIndex = 8;
            this.ModeOfWork.Text = "*Режим роботи*";
            // 
            // ClubForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(248, 235);
            this.ControlBox = false;
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.deleteLeague);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.yearOfFoundation);
            this.Controls.Add(this.leagueName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ModeOfWork);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximumSize = new System.Drawing.Size(264, 274);
            this.MinimumSize = new System.Drawing.Size(264, 274);
            this.Name = "ClubForm";
            this.ShowInTaskbar = false;
            this.Text = "*Режим роботи*";
            ((System.ComponentModel.ISupportInitialize)(this.yearOfFoundation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.Button deleteLeague;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown yearOfFoundation;
        private System.Windows.Forms.TextBox leagueName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label ModeOfWork;
    }
}