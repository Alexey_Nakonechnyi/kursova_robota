﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    [Serializable]
    public class League
    {
        private string name;
        public string Name
        {
            get => name;
            set => name = value;
        }
        private int yearOfFoundation;
        public int YearOfFoundation
        {
            get => yearOfFoundation;
            set => yearOfFoundation = value;
        }
        private List<Club> clubs;
        public List<Club> Clubs
        {
            get => clubs;
        }

        private List<Match> matches;
        public List<Match> Matches
        {
            get => matches;
        }
        public override string ToString()
        {
            return string.Copy(name);
        }

        public League()
        {
            name = "";
            clubs = new List<Club>();
            matches = new List<Match>();
        }
    }
}
