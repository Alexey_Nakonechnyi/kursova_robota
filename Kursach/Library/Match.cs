﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Library
{
    [Serializable]
    public class Match
    {
        private string name;
        public string Name
        {
            get => name;
            set => name = value;
        }
        private DateTime timeOfMatch;
        public DateTime TimeOfMatch
        {
            get => timeOfMatch;
            set => timeOfMatch = value;
        }
        private Club first;
        private Club second;
        public Club First
        {
            get => first;
            set => first = value;
        }
        public Club Second
        {
            get => second;
            set => second = value;
        }
        private League league;
        public League League
        {
            get => league;
            set => league = value;
        }
        public Match()
        {
            first = Club.UnknownClub;
            second = Club.UnknownClub;
        }
        public static List<Match> GenerateClubList(List<League> leagues)
        {
            List<Match> matches = new List<Match>();
            foreach (League league in leagues)
            {
                matches.AddRange(league.Matches);
            }
            return matches;
        }

        public override string ToString()
        {
            return $"{name}: <{first.Name}> - <{second.Name}>";
        }
    }
}
